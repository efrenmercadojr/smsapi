from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from course.models import Course


class Subject(models.Model):

    name = models.CharField(max_length=200)
    code = models.CharField(max_length=30)
    year_level = models.IntegerField(null=True, validators=[MinValueValidator(1), MaxValueValidator(7)])

    # Can optionally belong to a particular course
    courses = models.ManyToManyField(Course, related_name='subjects')


    def __str__(self):
        return self.code


    def clean(self, *args, **kwargs):
        # add custom validation here
        super(Subject, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Subject, self).save(*args, **kwargs)