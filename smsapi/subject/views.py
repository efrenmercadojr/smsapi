from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Subject
from .serializer import SubjectSerializer
from teacher.serializer import TeacherSerializer
from course.serializer import CourseSerializer


class SubjectViewSet(viewsets.ModelViewSet):
    """
    Subject list, create, retrieve, destroy, edit
    """
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


    @action(detail=True)
    def get_teachers(self, request, pk=None):
        """
        Get teachers that teach this subject
        """
        subject = self.get_object()
        serializer = TeacherSerializer(subject.teacher_set.all(), many=True, context={'request': request})
        return Response(serializer.data)

    @action(detail=True)
    def get_courses(self, request, pk=None):
        """
        Get courses the subject is being required
        """
        subject = self.get_object()
        serializer = CourseSerializer(subject.courses, many=True, context={'request': request})
        return Response(serializer.data)