from django.db import models


class Course(models.Model):

    name = models.CharField(max_length=200)
    code = models.CharField(max_length=30)

    def __str__(self):
        return self.code

    # Has a list of subjects required for it
    def get_required_subjects(self):
        return self.subject_set.all()