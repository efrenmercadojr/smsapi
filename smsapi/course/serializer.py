from rest_framework import serializers

from .models import Course


class CourseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'

class CourseMoreDetailedSerializer(CourseSerializer):

    number_of_students = serializers.IntegerField()
    number_of_teachers_under = serializers.IntegerField()