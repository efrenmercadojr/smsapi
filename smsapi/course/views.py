from django.shortcuts import render
from django.db.models import Count, Sum
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Course
from teacher.models import Teacher
from .serializer import CourseSerializer, CourseMoreDetailedSerializer


class CourseViewSet(viewsets.ModelViewSet):
    """
    Course list, create, retrieve, destroy, edit
    """
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


    @action(detail=False)
    def more_details(self, request):
        #courses = Course.objects.annotate(number_of_students_enrolled=Count(''))
        courses = self.get_queryset().annotate(number_of_students=Count('students', distinct=True),
                                                number_of_teachers_under=Count('subjects__teachers', distinct=True))
        
        serializer = CourseMoreDetailedSerializer(courses, many=True, context={'request': request})
        
        return Response(serializer.data)