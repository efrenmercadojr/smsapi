from rest_framework import serializers

from .models import Classg


class ClassgSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Classg
        fields = '__all__'


class ClassgWithStudentCountSerializer(ClassgSerializer):

    student_count = serializers.IntegerField()