from django.db import models
from django.db.models.signals import m2m_changed
from django.core.validators import MinValueValidator

from rest_framework.exceptions import ValidationError

from subject.models import Subject


class Classg(models.Model):

    name = models.CharField(max_length=50)
    max_capacity = models.PositiveIntegerField(validators=[MinValueValidator(1)])

    # Corresponds to one subject
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='classes')
    # Has one teacher assigned to it
    teacher = models.ForeignKey('teacher.Teacher', on_delete=models.CASCADE, related_name='classes')
    # Can have multiple students
    students = models.ManyToManyField('student.Student')


    def __str__(self):
        return self.name

    def clean(self, *args, **kwargs):
        
        # A teacher cannot be assigned to a class wherein the subject isn’t among the list of subject he/she can teach.
        if self.subject not in self.teacher.subjects.all():
            raise ValidationError('A teacher cannot be assigned to a class wherein the subject isn’t among the list of subject he/she can teach.')
        super(Classg, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Classg, self).save(*args, **kwargs)


#Signals
def students_changed(sender, **kwargs):

    students = kwargs['instance'].students.all()

    if kwargs['instance'].students.count() > kwargs['instance'].max_capacity:
        raise ValidationError("Class is already at maximum capacity.")

    for student in students:
        if student.year_level != kwargs['instance'].subject.year_level:
            raise ValidationError("A student cannot take a class if his/her year level is not the same as the required year level for the subject.")


m2m_changed.connect(students_changed, sender=Classg.students.through)