from django.shortcuts import render, get_object_or_404
from django.db.models import Count
from rest_framework import viewsets, renderers
from rest_framework.response import Response
from rest_framework.decorators import api_view, action

from .models import Classg
from .serializer import ClassgSerializer, ClassgWithStudentCountSerializer
from student.serializer import StudentSerializer
from student.models import Student


class ClassgViewSet(viewsets.ModelViewSet):
    """
    Class list, create, retrieve, destroy, edit
    """
    queryset = Classg.objects.all()
    serializer_class = ClassgSerializer

    @action(detail=True)
    def get_students(self, request, pk=None):
        classg = self.get_object()
        students = classg.students
        serializer = StudentSerializer(students, many=True, context={'request': request})
        return Response(serializer.data)

    @action(detail=False)
    def number_of_students(self, request):
        classes = Classg.objects.annotate(student_count=Count('students'))
        serializer = ClassgWithStudentCountSerializer(classes, many=True, context={'request': request})
        return Response(serializer.data)

    @action(methods=['put'], detail=True)
    def add_student(self, request, pk=None):
        classg = self.get_object()
        student = Student.objects.get(pk=request.data['id'])
        classg.students.add(student)
        classg.save()
        serializer = ClassgSerializer(classg, context={'request': request})
        return Response(serializer.data)

    @action(methods=['delete'], detail=True)
    def remove_student(self, request, pk=None):
        classg = self.get_object()
        student = Student.objects.get(pk=request.data['id'])
        classg.students.remove(student)
        classg.save()
        serializer = ClassgSerializer(classg, context={'request': request})
        return Response(serializer.data)