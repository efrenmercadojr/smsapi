from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

from course.models import Course
from classg.models import Classg
from teacher.models import Teacher


class Student(models.Model):

    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    year_level = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(7)])

    # Belongs to one course
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='students')

    def __str__(self):
        return self.firstname + " " + self.lastname

    # Can have multiple classes
    def get_classes(self):
        return self.classg_set.all()

    # Has a list of required subjects for a particular year level
    def get_required_subjects_for_students_course(self):
        # subject belongs to self.course
        return self.course.get_required_subjects()

    def get_teachers(self):
        classes = self.get_classes()
        teachers = Teacher.objects.filter(classes__in=classes).distinct()
        return teachers