from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import Student
from teacher.models import Teacher
from .serializer import StudentSerializer
from classg.serializer import ClassgSerializer
from subject.serializer import SubjectSerializer
from teacher.serializer import TeacherSerializer


class StudentViewSet(viewsets.ModelViewSet):
    """
    Student list, create, retrieve, destroy, edit
    """
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


    @action(detail=True)
    def get_classes(self, request, pk=None):
        student = self.get_object()
        serializer = ClassgSerializer(student.get_classes(), many=True, context={'request': request})
        return Response(serializer.data)

    @action(detail=True)
    def get_required_subjects(self, request, pk=None):
        student = self.get_object()
        subjects = student.get_required_subjects_for_students_course()
        serializer = SubjectSerializer(subjects, many=True, context={'request':request})
        return Response(serializer.data)

    @action(detail=True)
    def get_teachers(self, request, pk=None):
        student = self.get_object()
        teachers = student.get_teachers()
        serializer = TeacherSerializer(teachers, many=True, context={'request':request})
        return Response(serializer.data)