from django.shortcuts import render
from rest_framework import viewsets

from .models import Teacher
from .serializer import TeacherSerializer


class TeacherViewSet(viewsets.ModelViewSet):
    """
    Teacher list, create, retrieve, destroy, edit
    """
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer