from django.db import models

from subject.models import Subject
from classg.models import Classg


class Teacher(models.Model):

    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)

    # Can teach multiple subjects
    subjects = models.ManyToManyField(Subject, related_name='teachers')
    

    def __str__(self):
        return self.firstname + " " + self.lastname

    # Has multiple classes assigned to him/her
    def get_assigned_classes(self):
        return self.classg_set.all()

    