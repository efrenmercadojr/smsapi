from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url

from rest_framework import routers

from student.views import StudentViewSet
from teacher.views import TeacherViewSet
from subject.views import SubjectViewSet
from classg.views import ClassgViewSet #, add_or_delete_student
from course.views import CourseViewSet


router = routers.DefaultRouter()
router.register(r'students', StudentViewSet)
router.register(r'teachers', TeacherViewSet)
router.register(r'subjects', SubjectViewSet)
router.register(r'classes', ClassgViewSet)
router.register(r'courses', CourseViewSet)


urlpatterns = [
    #url(r'^classes/(?P<pk>[0-9]+)/students/(?P<studentPk>[0-9]+)$', add_or_delete_student),
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^', include(router.urls)),
]
