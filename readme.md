# To run locally:







```
pip install pipenv
pipenv install
pipenv shell
cd smsapi
python manage.py migrate
python manage.py runserver
```


Go to localhost:8000






# Endpoints



## GET POST



/students/

/teachers/

/courses/

/subjects/

/classes/




## GET PUT DELETE



/students/{id}

/teachers/{id}

/courses/{id}

/subjects/{id}

/classes/{id}



## PUT



```json
{ "id": 1 }
```

/classes/{id}/add_student



## DELETE



```json
{ "id": 1 }
```

/classes/{id}/remove_student



## GET

/students/{id}/get_classes

/students/{id}/get_required_subjects

/students/{id}/get_teachers 

/classes/{id}/get_students 

/subjects/{id}/get_teachers 

/subjects/{id}/get_courses 


/classes/count_students 

/courses/count_students_teachers 
